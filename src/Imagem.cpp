#include "Imagem.hpp"
#include <iostream>
#include <sstream>

using namespace std;

Imagem::Imagem(){
}

Imagem::~Imagem(){
}

string Imagem::getTipo(){
	return tipo;
}

void Imagem::setTipo(string linha){
	tipo = linha;
}

string Imagem::getAltura(){
	return altura;
}

void Imagem::setAltura(string linha){
	stringstream line(linha);
	line >> altura;
}

string Imagem::getLargura(){
	return largura;
}

void Imagem::setLargura(string linha){
	string trash;
	stringstream line(linha);
	line >> trash;
	line >> largura;
}

string Imagem::getEscala(){
	return escala;
}

void Imagem::setEscala(string linha){
	escala = linha;
}

string Imagem::getComentario(){
	return comentario;
}

void Imagem::setComentario(string linha){
	comentario = linha;
}


