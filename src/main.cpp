#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "Imagem.hpp"
using namespace std;

int main() {
	Imagem Imagem_Retorno;
        string alvo,linha;
	int i = 0;

        cout << "Digite o nome do Arquivo Alvo: ";
        cin >> alvo;

        alvo = "./doc/" alvo + ".ppm";

       ifstream inputfile(alvo.c_str());

	if(not inputfile.is_open()){
                cout << "Erro ao tentar abrir o Arquivo " + alvo + ".ppm" << endl;
        return -1;
        }

	ofstream outputfile("./doc/Imagem_Retorno.ppm");
        
        while(getline(inputfile, linha)) {
                if(linha[0] != '#'){
		if(i<=2){
                        switch(i){
                                case 0:
                                        Imagem_Retorno.setTipo(linha);

                                        cout << "Formato do Arquivo Alvo: " << Imagem_Retorno.getTipo() << endl;
                                        outputfile << Imagem_Retorno.getTipo << endl;
                                        i++;
                                break;
                                case 1:
					novaImagem.setAltura(linha);
					novaImagem.setLargura(linha);

                                        cout << "Altura do Arquivo Alvo: " << novaImagem.setAltura(linha)  << " " << "Largura do Arquivo Alvo: " << novaImagem.setLargura(linha) << endl;
                                        outputfile << novaImagem.setAltura(linha) << " " << novaImagem.setLargura(linha) << endl;
                                        i++;
                                break;
                                case 2:                                        						novaImagem.setEscala(linha);

                                        cout << "Escala maxima do Arquivo Alvo: " << novaImagem.setEscala(linha); << endl;
                                        outputfile << novaImagem.setEscala(linha); << endl;
                                        i++;
                                break;
                        }

		}
                else{
                        outputfile << linha << endl;
		}
	}
	else{
		Imagem_Retorno.setComentario(linha);
		outputfile << Imagem_Retorno.getComentario()<<endl;
	}
}

	inputfile.close();
	return 0;
}
