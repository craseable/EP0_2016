#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>

using namespace std;

class Imagem{
private:
	string tipo;
	string altura;
	string largura;
	string escala;
	string comentario;
public:
	Imagem();
	~Imagem();
	string getTipo();
	void setTipo(string linha);
	string getAltura();
	void setAltura(string linha);
	string getLargura();
	void setLargura(string linha);
	string getEscala();
	void setEscala(string linha);
	string getComentario();
	void setComentario(string linha);
};

#endif


